﻿using MySql.Data.MySqlClient;
using System;
using Periciando.Domain;
using Periciando.Utils;
using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;

namespace Periciando.Persistence
{
    public class PericiandoDAO
    {
        private readonly string StringConexao = "Server=localhost;Database=periciando;Uid=root;Pwd=1234567;";
        private MySqlConnection Conexao { get; }

        public PericiandoDAO()
        {
            Conexao = new MySqlConnection(StringConexao);
        }

        public List<Object> SelectAll(string entityName)
        {
            Conexao.Open();

            string query = SqlFormatter.GenerateSelectAllString(entityName);

            MySqlCommand comando = NewSQLCommand(query);

            MySqlDataReader reader = comando.ExecuteReader();

            if (!reader.HasRows)
            {
                Conexao.Close();
                throw new Exception($"Nenhum {entityName} encontrado.");
            }

            List<Object> list = ReadAll(reader, entityName);

            Conexao.Close();

            return list;
        }

        public T SelectById<T>(int id, string entityName, Object obj)
        {
            Conexao.Open();

            string query = SqlFormatter.GenerateSelectByIdString(entityName);

            MySqlCommand comando = NewSQLCommand(query);

            comando.Parameters.AddWithValue("Id", id);

            MySqlDataReader reader = comando.ExecuteReader();


            if (!reader.HasRows)
            {
                Conexao.Close();
                throw new Exception($"Nenhum {entityName} encontrado.");
            }

            obj = Read(reader, entityName, obj);

            Conexao.Close();

            return (T)obj;
        }

        public List<Especialidade> SelectPeritoEspecialidades(string peritoType, string idType, int id)
        {
            List<Especialidade> especialidades = new List<Especialidade>();
            Conexao.Open();

            string query = SqlFormatter.GenerateSelectEspecialidadesString(peritoType, idType);

            MySqlCommand comando = NewSQLCommand(query);

            comando.Parameters.AddWithValue("Id", id);

            MySqlDataReader reader = comando.ExecuteReader();


            if (!reader.HasRows)
            {
                Conexao.Close();
                return especialidades;
            }

            while(reader.Read())
            {
                especialidades.Add(new Especialidade(reader.GetInt32("id_especialidade"), reader.GetString("nome")));
            }

            Conexao.Close();

            return especialidades;
        }

        public List<Object> SelectPeritosByEspecialidade(string peritoType, string idType, int id_especialidade)
        {
            List<Object> peritos = new();
            Conexao.Open();

            string query = SqlFormatter.GenerateSelectPeritosByEspecialidadeString(peritoType, idType);

            MySqlCommand comando = NewSQLCommand(query);

            comando.Parameters.AddWithValue("Id", id_especialidade);

            MySqlDataReader reader = comando.ExecuteReader();

            if (!reader.HasRows)
            {
                Conexao.Close();
                return peritos;
            }

            while (reader.Read())
            {
                peritos.Add(new Usuario(reader.GetInt32($"id_{idType}"), reader.GetString("nome"), 
                    reader.GetString("email"), reader.GetString("telefone")));
            }

            Conexao.Close();

            return peritos;
        }

        private Object Read(MySqlDataReader reader, string entityName, Object obj)
        {
            while (reader.Read())
            {
                switch (entityName)
                {
                    case "perito_fisico":
                        obj = new Perito_Fisico(reader.GetString("cpf"), reader.GetString("nome"),
                            reader.GetString("email"), "", reader.GetString("genero"),
                            reader.GetDateTime("dt_Nascimento"), reader.GetString("telefone"), new Endereco(reader.GetInt32("id_endereco")));
                        break;
                    case "perito_juridico":
                        obj = new Perito_Juridico(reader.GetString("cnpj"), reader.GetString("nome"),
                            reader.GetString("email"), "", reader.GetString("genero"),
                            reader.GetDateTime("dt_Nascimento"), reader.GetString("telefone"), new Endereco(reader.GetInt32("id_endereco")));
                        break;
                    case "cliente_fisico":
                        obj = new Cliente_Fisico(reader.GetString("cpf"), reader.GetString("nome"),
                            reader.GetString("email"), "", reader.GetString("genero"),
                            reader.GetDateTime("dt_Nascimento"), reader.GetString("telefone"), new Endereco(reader.GetInt32("id_endereco")));
                        break;
                    case "cliente_juridico":
                        obj = new Cliente_Juridico(reader.GetString("cnpj"), reader.GetString("nome"),
                            reader.GetString("email"), "", reader.GetString("genero"),
                            reader.GetDateTime("dt_Nascimento"), reader.GetString("telefone"), new Endereco(reader.GetInt32("id_endereco")));
                        break;
                    case "pagamento_perito_fisico":
                        obj = new Pagamento_Perito_Fisico(reader.GetString("codigo_transacao"),
                            reader.GetDateTime("data_transacao"), reader.GetFloat("valor"), reader.GetString("tipo_de_pagamento"),
                            new Perito_Fisico(reader.GetInt32("id_perito_fisico")));
                        break;
                    case "pagamento_perito_juridico":
                        obj = new Pagamento_Perito_Juridico(reader.GetString("codigo_transacao"),
                            reader.GetDateTime("data_transacao"), reader.GetFloat("valor"), reader.GetString("tipo_de_pagamento"),
                            new Perito_Juridico(reader.GetInt32("id_perito_juridico")));
                    break;
                    case "endereco":
                        obj = new Endereco(reader.GetInt32("cep"), reader.GetString("rua"),
                            reader.GetString("bairro"), reader.GetString("cidade"), reader.GetString("pais"));
                        break;
                    default:
                        break;
                }
            }
            return obj;
        }

        private List<Object> ReadAll(MySqlDataReader reader, string entityName)
        {
            List<Object> list = new List<Object>();
            while (reader.Read())
            {
                switch (entityName)
                {
                    case "perito_fisico":
                        list.Add(new Perito_Fisico(reader.GetString("cpf"), reader.GetInt32("id_perito_fisico"), reader.GetString("nome"),
                            reader.GetString("email"), "", reader.GetString("genero"),
                            reader.GetDateTime("dt_Nascimento"), reader.GetString("telefone"), new Endereco(reader.GetInt32("id_endereco"))));
                        break;
                    case "perito_juridico":
                        list.Add(new Perito_Juridico(reader.GetString("cnpj"), reader.GetInt32("id_perito_juridico"), reader.GetString("nome"),
                            reader.GetString("email"), "", reader.GetString("genero"),
                            reader.GetDateTime("dt_Nascimento"), reader.GetString("telefone"), new Endereco(reader.GetInt32("id_endereco"))));
                        break;
                    case "cliente_fisico":
                        list.Add(new Cliente_Fisico(reader.GetString("cpf"), reader.GetInt32("id_cliente_fisico"), reader.GetString("nome"),
                            reader.GetString("email"), "", reader.GetString("genero"),
                            reader.GetDateTime("dt_Nascimento"), reader.GetString("telefone"), new Endereco(reader.GetInt32("id_endereco"))));
                        break;
                    case "cliente_juridico":
                        list.Add(new Cliente_Juridico(reader.GetString("cnpj"), reader.GetInt32("id_cliente_juridico"), reader.GetString("nome"),
                            reader.GetString("email"), "", reader.GetString("genero"),
                            reader.GetDateTime("dt_Nascimento"), reader.GetString("telefone"), new Endereco(reader.GetInt32("id_endereco"))));
                        break;
                    case "pagamento_perito_fisico":
                        list.Add(new Pagamento_Perito_Fisico(reader.GetInt32("id_pagamento_perito_fisico"), reader.GetString("codigo_transacao"),
                            reader.GetDateTime("data_transacao"), reader.GetFloat("valor"), reader.GetString("tipo_de_pagamento"),
                            new Perito_Fisico(reader.GetInt32("id_perito_fisico"))));
                        break;
                    case "pagamento_perito_juridico":
                        list.Add(new Pagamento_Perito_Juridico(reader.GetInt32("id_pagamento_perito_juridico"), reader.GetString("codigo_transacao"),
                            reader.GetDateTime("data_transacao"), reader.GetFloat("valor"), reader.GetString("tipo_de_pagamento"),
                            new Perito_Juridico(reader.GetInt32("id_perito_juridico"))));
                        break;
                    case "endereco":
                        list.Add(new Endereco(reader.GetInt32("id_endereco"), reader.GetInt32("cep"), reader.GetString("rua"),
                            reader.GetString("bairro"), reader.GetString("cidade"), reader.GetString("pais")));
                        break;
                }
            }

            return list;
        }

        public void InsertInheritance(Object obj)
        {
            Conexao.Open();

            string query = SqlFormatter.GenerateInsertString(obj);

            List<MySqlParameter> mySqlParameters = SqlFormatter.GenerateMySqlParamsInheritance(obj);

            MySqlCommand comando = NewSQLCommand(query);

            foreach (MySqlParameter mySqlParameter in mySqlParameters)
            {
                comando.Parameters.Add(mySqlParameter);
            }

            comando.ExecuteNonQuery();

            Conexao.Close();
        }


        public int Insert(Object obj)
        {
            try
            {
                Conexao.Open();

                string query = SqlFormatter.GenerateInsertString(obj);

                List<MySqlParameter> mySqlParameters = SqlFormatter.GenerateMySqlParams(obj);


                MySqlCommand comando = NewSQLCommand(query);


                foreach (MySqlParameter mySqlParameter in mySqlParameters)
                {

                    comando.Parameters.Add(mySqlParameter);
                }

                comando.ExecuteNonQuery();

                int lastID = (int)comando.LastInsertedId;

                Conexao.Close();

                return lastID;
            } catch(MySqlException err)
            {
                Conexao.Close();
                throw new Exception(err.Message);
            }
        }


        public void Delete(int id, string entityName)
        {
            Conexao.Open();

            string query = SqlFormatter.GenerateDeleteString(entityName);

            MySqlParameter deleteParam = new MySqlParameter("Id", id);

            MySqlCommand comando = NewSQLCommand(query);

            comando.Parameters.Add(deleteParam);

            comando.ExecuteNonQuery();

            Conexao.Close();
        }

        public void UpdateSenha(int id, string entityName, string senha)
        {
            Conexao.Open();

            string query = SqlFormatter.GenerateUpdateSenhaString(entityName);

            MySqlParameter idParam = new MySqlParameter("Id", id);
            MySqlParameter senhaParam = new MySqlParameter("Senha", senha);

            MySqlCommand comando = NewSQLCommand(query);

            comando.Parameters.Add(idParam);
            comando.Parameters.Add(senhaParam);

            comando.ExecuteNonQuery();

            Conexao.Close();
        }

        public void UpdateEndereco(Endereco endereco)
        {
            Conexao.Open();

            string query = SqlFormatter.GenerateUpdateEnderecoString();

            MySqlCommand comando = NewSQLCommand(query);

            comando.Parameters.AddWithValue("Id", endereco.Id);
            comando.Parameters.AddWithValue("Bairro", endereco.Bairro);
            comando.Parameters.AddWithValue("Pais", endereco.Pais);
            comando.Parameters.AddWithValue("Cidade", endereco.Cidade);
            comando.Parameters.AddWithValue("Cep", endereco.Cep);
            comando.Parameters.AddWithValue("Rua", endereco.Rua);

            comando.ExecuteNonQuery();

            Conexao.Close();
        }

        private MySqlCommand NewSQLCommand(string query)
        {
            return new MySqlCommand(query, Conexao);
        }
    }
}
