﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Periciando.Domain
{
    public class Perito_Fisico_Especialidade
    {
        public int Id { get; set; }
        public Perito_Fisico Perito_Fisico { get; set; }
        public Especialidade Especialidade { get; set; }

        public Perito_Fisico_Especialidade()
        {

        }

        public Perito_Fisico_Especialidade(Perito_Fisico perito_Fisico, Especialidade especialidade)
        {
            Perito_Fisico = perito_Fisico;
            Especialidade = especialidade;
        }
    }
}
