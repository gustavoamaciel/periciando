using System;

namespace Periciando.Domain
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Genero { get; set; }
        public DateTime Dt_nascimento { get; set; }
        public string Telefone { get; set; }
        public Endereco Endereco { get; set; }


        public Usuario()
        {
        }

        public Usuario(int id, string nome, string email, string telefone)
        {
            Id = id;
            Nome = nome;
            Email = email;
            Telefone = telefone;
        }

        public Usuario(int id)
        {
            Id = id;
        }


        public Usuario(int id, string nome, string email, string senha, string genero, DateTime dt_nascimento, string telefone, Endereco endereco)
        {
            Id = id;
            Nome = nome;
            Email = email;
            Senha = senha;
            Genero = genero;
            Dt_nascimento = dt_nascimento;
            Telefone = telefone;
            Endereco = endereco;
        }

        public Usuario(string nome, string email, string senha, string genero, DateTime dt_nascimento, string telefone, Endereco endereco)
        {
            Nome = nome;
            Email = email;
            Senha = senha;
            Genero = genero;
            Dt_nascimento = dt_nascimento;
            Telefone = telefone;
            Endereco = endereco;
        }
    }
}
