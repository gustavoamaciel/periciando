﻿using System;
using System.Collections.Generic;

namespace Periciando.Domain
{
    public class Perito_Fisico : Usuario
    {
        public string Cpf { get; set; }
        public List<Pagamento_Perito_Fisico> Pagamentos { get; } = new List<Pagamento_Perito_Fisico>();
        public List<Especialidade> Especialidades { get; } = new List<Especialidade>();


        public Perito_Fisico()
        {

        }

        public Perito_Fisico(int id) : base(id)
        {

        }

        public Perito_Fisico(string cpf, string nome, string email, string senha, string genero, DateTime dt_nascimento, string telefone, Endereco endereco) : base(nome, email, senha, genero, dt_nascimento, telefone, endereco)
        {
            Cpf = cpf;
        }

        public Perito_Fisico(string cpf, int id, string nome, string email, string senha, string genero, DateTime dt_nascimento, string telefone, Endereco endereco) : base(id, nome, email, senha, genero, dt_nascimento, telefone, endereco)
        {
            Cpf = cpf;
        }
    }
}
