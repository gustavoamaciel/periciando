﻿using System;

namespace Periciando.Domain
{
    public class Cliente_Juridico : Usuario
    {
        public string Cnpj { get; set; }

        public Cliente_Juridico()
        {

        }

        public Cliente_Juridico(string cnpj, string nome, string email, string senha, string genero, DateTime dt_nascimento, string telefone, Endereco endereco) : base(nome, email, senha, genero, dt_nascimento, telefone, endereco)
        {
            Cnpj = cnpj;
        }

        public Cliente_Juridico(string cnpj, int id, string nome, string email, string senha, string genero, DateTime dt_nascimento, string telefone, Endereco endereco) : base(id, nome, email, senha, genero, dt_nascimento, telefone, endereco)
        {
            Cnpj = cnpj;
        }
    }
}
