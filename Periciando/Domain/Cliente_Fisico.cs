﻿using System;

namespace Periciando.Domain
{
    public class Cliente_Fisico : Usuario
    {
        public string Cpf { get; set; }

        public Cliente_Fisico()
        {

        }

        public Cliente_Fisico(string cpf, string nome, string email, string senha, string genero, DateTime dt_nascimento, string telefone, Endereco endereco) : base(nome, email, senha, genero, dt_nascimento, telefone, endereco)
        {
            Cpf = cpf;
        }

        public Cliente_Fisico(string cpf, int id, string nome, string email, string senha, string genero, DateTime dt_nascimento, string telefone, Endereco endereco) : base(id, nome, email, senha, genero, dt_nascimento, telefone, endereco)
        {
            Cpf = cpf;
        }
    }
}
