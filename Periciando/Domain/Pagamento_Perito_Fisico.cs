﻿using System;

namespace Periciando.Domain
{

    public class Pagamento_Perito_Fisico
    {
        public int Id { get; set; }
        public string Codigo_Transacao { get; set; }
        public DateTime Data_Transacao { get; set; }
        public float Valor { get; set; }
        public string Tipo_De_Pagamento { get; set; }
        public Perito_Fisico Perito_Fisico { get; set; }

        public Pagamento_Perito_Fisico ()
        {

        }

        public Pagamento_Perito_Fisico(int id, string codigotransicao, DateTime dataTransacao, float valor, string tipodepagamento, Perito_Fisico perito)
        {
            Id = id;
            Codigo_Transacao = codigotransicao;
            Data_Transacao = dataTransacao;
            Valor = valor;
            Tipo_De_Pagamento = tipodepagamento;
            Perito_Fisico = perito;

        }

        public Pagamento_Perito_Fisico(string codigotransicao, DateTime dataTransacao, float valor, string tipodepagamento, Perito_Fisico perito)
        {

            Codigo_Transacao = codigotransicao;
            Data_Transacao = dataTransacao;
            Valor = valor;
            Tipo_De_Pagamento = tipodepagamento;
            Perito_Fisico = perito;

        }

    }
}