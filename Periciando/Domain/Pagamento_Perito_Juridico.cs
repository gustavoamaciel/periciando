﻿using System;

namespace Periciando.Domain
{

    public class Pagamento_Perito_Juridico
    {
        public int Id { get; set; }
        public string Codigo_Transacao { get; set; }
        public DateTime Data_Transacao { get; set; }
        public float Valor { get; set; }
        public string Tipo_De_Pagamento { get; set; }
        public Perito_Juridico Perito_Juridico { get; set; }

        public Pagamento_Perito_Juridico()
        {

        }

        public Pagamento_Perito_Juridico(int id, string codigotransicao, DateTime dataTransacao, float valor, string tipodepagamento, Perito_Juridico perito)
        {
            Id = id;
            Codigo_Transacao = codigotransicao;
            Data_Transacao = dataTransacao;
            Valor = valor;
            Tipo_De_Pagamento = tipodepagamento;
            Perito_Juridico = perito;

        }

        public Pagamento_Perito_Juridico(string codigotransicao, DateTime dataTransacao, float valor, string tipodepagamento, Perito_Juridico perito)
        {

            Codigo_Transacao = codigotransicao;
            Data_Transacao = dataTransacao;
            Valor = valor;
            Tipo_De_Pagamento = tipodepagamento;
            Perito_Juridico = perito;

        }

    }
}