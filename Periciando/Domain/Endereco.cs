namespace Periciando.Domain
{
    public class Endereco
    {
        public int Id { get; set;  }
        public int Cep { get; set; }
        public string Rua { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Pais { get; set; }

        public Endereco()
        {
        }

        public Endereco(int id)
        {
            Id = id;
        }

        public Endereco(int id, int cep, string rua, string bairro, string cidade, string pais)
        {
            Id = id;
            Cep = cep;
            Rua = rua;
            Bairro = bairro;
            Cidade = cidade;
            Pais = pais;
        }

        public Endereco(int cep, string rua, string bairro, string cidade, string pais)
        {
            Cep = cep;
            Rua = rua;
            Bairro = bairro;
            Cidade = cidade;
            Pais = pais;
        }
    }


}

