﻿using System;
using System.Collections.Generic;

namespace Periciando.Domain
{
    public class Perito_Juridico : Usuario
    {
        public string Cnpj { get; set; }
        public List<Pagamento_Perito_Juridico> Pagamentos { get; } = new List<Pagamento_Perito_Juridico>();
        public List<Especialidade> Especialidades { get; } = new List<Especialidade>();

        public Perito_Juridico()
        {

        }

        public Perito_Juridico(int id) : base(id)
        {

        }

        public Perito_Juridico(string cnpj, string nome, string email, string senha, string genero, DateTime dt_nascimento, string telefone, Endereco endereco) : base(nome, email, senha, genero, dt_nascimento, telefone, endereco)
        {
            Cnpj = cnpj;
        }

        public Perito_Juridico(string cnpj, int id, string nome, string email, string senha, string genero, DateTime dt_nascimento, string telefone, Endereco endereco) : base(id, nome, email, senha, genero, dt_nascimento, telefone, endereco)
        {
            Cnpj = cnpj;
        }
    }
}
