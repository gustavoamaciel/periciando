﻿namespace Periciando.Domain
{
    public class Especialidade
    {
        public int Id { get; set; }
        public  string Nome { get; set; }

        public Especialidade()
        {

        }

        public Especialidade(int id)
        {
            Id = id;
        }

        public Especialidade(int id, string nome)
        {
            Id = id;
            Nome = nome;
        }
    }


}
