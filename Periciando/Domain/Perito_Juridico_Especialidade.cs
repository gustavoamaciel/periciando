﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Periciando.Domain
{
    public class Perito_Juridico_Especialidade
    {
        public int Id { get; set; }
        public Perito_Juridico Perito_Juridico { get; set; }
        public Especialidade Especialidade { get; set; }

        public Perito_Juridico_Especialidade()
        {

        }

        public Perito_Juridico_Especialidade(Perito_Juridico perito_Juridico, Especialidade especialdiade)
        {
            Perito_Juridico = perito_Juridico;
            Especialidade = especialdiade;
        }
    }
}
