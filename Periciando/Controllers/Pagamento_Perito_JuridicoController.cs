﻿using Microsoft.AspNetCore.Mvc;
using Periciando.Domain;
using Periciando.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Periciando.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class Pagamento_Perito_JuridicoController : ControllerBase
    {
        Pagamento_Perito_JuridicoService service = new Pagamento_Perito_JuridicoService();

        [HttpGet("{id}")]
        public IActionResult GetPagamento_Perito_Juridico([FromRoute] int id)
        {
            try
            {
                Pagamento_Perito_Juridico pagamento_perito_juridico = service.ReadPagamento_Perito_Juridico(id);

                return Ok(pagamento_perito_juridico);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetPagamentos_Peritos_Juridicos()
        {
            try
            {
                List<Object> pagamentos_peritos_juridicos = service.ReadAllPagamentos_Peritos_Juridicos();

                return Ok(pagamentos_peritos_juridicos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult PostPagamento_Perito_Juridico([FromBody] Pagamento_Perito_Juridico pagamento_perito_juridico)
        {
            try
            {
                service.CreatePagamento_Perito_Juridico(pagamento_perito_juridico);

                return Created($"api/pagamento_perito_juridico/", $"Pagamento_Perito_Juridico {pagamento_perito_juridico.Codigo_Transacao} criado.");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
