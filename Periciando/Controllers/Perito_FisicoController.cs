﻿using Microsoft.AspNetCore.Mvc;
using Periciando.Domain;
using Periciando.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Periciando.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class Perito_FisicoController : ControllerBase
    {
        Perito_FisicoService peritoFisicoService = new Perito_FisicoService();
        EnderecoService enderecoService = new EnderecoService();

        [HttpGet("{id}")]
        public IActionResult GetPerito_Fisico([FromRoute] int id)
        {
            try
            {
                Perito_Fisico perito_fisico = peritoFisicoService.ReadPerito_Fisico(id);

                return Ok(perito_fisico);
            } catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetPeritos_Fisicos()
        {
            try
            {
                List<Object> peritos_fisicos = peritoFisicoService.ReadAllPeritos_Fisicos();

                return Ok(peritos_fisicos);
            } catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult PostPerito_Fisico([FromBody] Perito_Fisico perito_fisico)
        {
            int enderecoID = enderecoService.CreateEndereco(perito_fisico.Endereco);

            perito_fisico.Endereco.Id = enderecoID;

            peritoFisicoService.CreatePerito_Fisico(perito_fisico);

            return Created($"api/perito_fisico/", $"Perito_Fisico {perito_fisico.Nome} criado.");
        }

        [HttpDelete("{id_perito}/{id_endereco}")]
        public IActionResult DeletePerito_Fisico([FromRoute] int id_perito, [FromRoute] int id_endereco)
        {
            peritoFisicoService.DeletePerito_Fisico(id_perito);
            enderecoService.DeleteEndereco(id_endereco);

            return NoContent();
        }

        [HttpPut("{id}")]
        public IActionResult UpdatePerito_FisicoSenha([FromRoute] int id, [FromBody] string senha)
        {
            peritoFisicoService.UpdateSenha(id, senha);

            return Ok("Senha atualizada!");
        }

        [HttpPut("endereço")]
        public IActionResult UpdatePerito_FisicoEndereco([FromBody] Endereco endereco)
        {
            enderecoService.UpdateEndereco(endereco);

            return Ok("Endereço atualizado!");
        }

        [HttpPost("especialidade/{id_perito}/{id_especialidade}")]
        public IActionResult AddEspecialidade([FromRoute] int id_perito, [FromRoute] int id_especialidade)
        {
            try
            {
                peritoFisicoService.AddEspecialidade(id_perito, id_especialidade);

                return Ok("Especialidade adicionada");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
