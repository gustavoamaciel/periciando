﻿using Microsoft.AspNetCore.Mvc;
using Periciando.Domain;
using Periciando.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Periciando.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class Pagamento_Perito_FisicoController : ControllerBase
    {
        Pagamento_Perito_FisicoService service = new Pagamento_Perito_FisicoService();

        [HttpGet("{id}")]
        public IActionResult GetPagamento_Perito_Fisico([FromRoute] int id)
        {
            try
            {
                Pagamento_Perito_Fisico pagamento_perito_fisico = service.ReadPagamento_Perito_Fisico(id);

                return Ok(pagamento_perito_fisico);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetPagamentos_Peritos_Fisicos()
        {
            try
            {
                List<Object> pagamentos_peritos_fisicos = service.ReadAllPagamentos_Peritos_Fisicos();

                return Ok(pagamentos_peritos_fisicos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult PostPagamento_Perito_Fisico([FromBody] Pagamento_Perito_Fisico pagamento_perito_fisico)
        {
            try
            {
                service.CreatePagamento_Perito_Fisico(pagamento_perito_fisico);

                return Created($"api/pagamento_perito_fisico/", $"Pagamento_Perito_Fisico {pagamento_perito_fisico.Codigo_Transacao} criado.");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
