﻿using Microsoft.AspNetCore.Mvc;
using Periciando.Domain;
using Periciando.Persistence;
using Periciando.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Periciando.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EspecialidadeController : ControllerBase
    {
        EspecialidadeService service = new EspecialidadeService();

        [HttpGet("{id}")]
        public IActionResult GetPeritosByEspecialidade([FromRoute] int id)
        {
            List<Object> peritos = service.GetPeritosByEspecialidade(id);

            return Ok(peritos);
        }

        [HttpPost]
        public IActionResult PostEspecialidade([FromBody] Especialidade especialidade)
        {
            service.CreateEspecialidade(especialidade);

            return Created($"api/especialidades/", $"Especialidade {especialidade.Nome} criada.");
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteEspecialidade([FromRoute] int id)
        {
            service.DeleteEspecialidade(id);

            return NoContent();
        }

    }
}
