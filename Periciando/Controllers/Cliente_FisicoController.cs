﻿using Microsoft.AspNetCore.Mvc;
using Periciando.Domain;
using Periciando.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Periciando.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class Cliente_FisicoController : ControllerBase
    {
        Cliente_FisicoService clienteFisicoService = new Cliente_FisicoService();
        EnderecoService enderecoService = new EnderecoService();

        [HttpGet("{id}")]
        public IActionResult GetCliente_Fisico([FromRoute] int id)
        {
            try
            {
                Cliente_Fisico cliente_fisico = clienteFisicoService.ReadCliente_Fisico(id);

                return Ok(cliente_fisico);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetClientes_Fisicos()
        {
            try
            {
                List<Object> clientes_fisicos = clienteFisicoService.ReadAllClientes_Fisicos();

                return Ok(clientes_fisicos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult PostCliente_Fisico([FromBody] Cliente_Fisico cliente_fisico)
        {
            int enderecoID = enderecoService.CreateEndereco(cliente_fisico.Endereco);

            cliente_fisico.Endereco.Id = enderecoID;

            clienteFisicoService.CreateCliente_Fisico(cliente_fisico);

            return Created($"api/cliente_fisico/", $"Cliente_Fisico {cliente_fisico.Nome} criado.");
        }

        [HttpDelete("{id_cliente}/{id_endereco}")]
        public IActionResult DeleteCliente_Fisico([FromRoute] int id_cliente, [FromRoute] int id_endereco)
        {
            clienteFisicoService.DeleteCliente_Fisico(id_cliente);
            enderecoService.DeleteEndereco(id_endereco);

            return NoContent();
        }

        [HttpPut("{id}")]
        public IActionResult UpdateCliente_FisicoSenha([FromRoute] int id, [FromBody] string senha)
        {
            clienteFisicoService.UpdateSenha(id, senha);

            return Ok("Senha atualizada!");
        }

        [HttpPut("endereço")]
        public IActionResult UpdateCliente_FisicoEndereco([FromBody] Endereco endereco)
        {
            enderecoService.UpdateEndereco(endereco);

            return Ok("Endereço atualizado!");
        }

    }
}
