﻿using Microsoft.AspNetCore.Mvc;
using Periciando.Domain;
using Periciando.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Periciando.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class Cliente_JuridicoController : ControllerBase
    {
        Cliente_JuridicoService clienteJuridicoService = new Cliente_JuridicoService();
        EnderecoService enderecoService = new EnderecoService();

        [HttpGet("{id}")]
        public IActionResult GetCliente_Juridico([FromRoute] int id)
        {
            try
            {
                Cliente_Juridico cliente_juridico = clienteJuridicoService.ReadCliente_Juridico(id);

                return Ok(cliente_juridico);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetClientes_Juridicos()
        {
            try
            {
                List<Object> clientes_juridicos = clienteJuridicoService.ReadAllClientes_Juridicos();

                return Ok(clientes_juridicos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult PostCliente_Juridico([FromBody] Cliente_Juridico cliente_juridico)
        {
            int enderecoID = enderecoService.CreateEndereco(cliente_juridico.Endereco);

            cliente_juridico.Endereco.Id = enderecoID;

            clienteJuridicoService.CreateCliente_Juridico(cliente_juridico);

            return Created($"api/cliente_juridico/", $"Cliente_Juridico {cliente_juridico.Nome} criado.");
        }

        [HttpDelete("{id_cliente}/{id_endereco}")]
        public IActionResult DeleteCliente_Juridico([FromRoute] int id_cliente, [FromRoute] int id_endereco)
        {
            clienteJuridicoService.DeleteCliente_Juridico(id_cliente);
            enderecoService.DeleteEndereco(id_endereco);

            return NoContent();
        }

        [HttpPut("{id}")]
        public IActionResult UpdateCliente_JuridicoSenha([FromRoute] int id, [FromBody] string senha)
        {
            clienteJuridicoService.UpdateSenha(id, senha);

            return Ok("Senha atualizada!");
        }

        [HttpPut("endereço")]
        public IActionResult UpdateCliente_JuridicoEndereco([FromBody] Endereco endereco)
        {
            enderecoService.UpdateEndereco(endereco);

            return Ok("Endereço atualizado!");
        }
    }
}
