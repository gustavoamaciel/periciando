﻿using Microsoft.AspNetCore.Mvc;
using Periciando.Domain;
using Periciando.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Periciando.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class Perito_JuridicoController : ControllerBase
    {
        Perito_JuridicoService peritoJuridicoService = new Perito_JuridicoService();
        EnderecoService enderecoService = new EnderecoService();

        [HttpGet("{id}")]
        public IActionResult GetPerito_Juridico([FromRoute] int id)
        {
            try
            {
                Perito_Juridico perito_juridico = peritoJuridicoService.ReadPerito_Juridico(id);

                return Ok(perito_juridico);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetPeritos_Juridicos()
        {
            try
            {
                List<Object> peritos_juridicos = peritoJuridicoService.ReadAllPeritos_Juridicos();

                return Ok(peritos_juridicos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult PostPerito_Juridico([FromBody] Perito_Juridico perito_juridico)
        {
            int enderecoID = enderecoService.CreateEndereco(perito_juridico.Endereco);

            perito_juridico.Endereco.Id = enderecoID;

            peritoJuridicoService.CreatePerito_Juridico(perito_juridico);

            return Created($"api/perito_juridico/", $"Perito_Juridico {perito_juridico.Nome} criado.");
        }

        [HttpDelete("{id_perito}/{id_endereco}")]
        public IActionResult DeletePerito_Juridico([FromRoute] int id_perito, [FromRoute] int id_endereco)
        {
            peritoJuridicoService.DeletePerito_Juridico(id_perito);
            enderecoService.DeleteEndereco(id_endereco);

            return NoContent();
        }

        [HttpPut("{id}")]
        public IActionResult UpdatePerito_JuridicoSenha([FromRoute] int id, [FromBody] string senha)
        {
            peritoJuridicoService.UpdateSenha(id, senha);

            return Ok("Senha atualizada!");
        }

        [HttpPut("endereço")]
        public IActionResult UpdatePerito_JuridicoEndereco([FromBody] Endereco endereco)
        {
            enderecoService.UpdateEndereco(endereco);

            return Ok("Endereço atualizado!");
        }

        [HttpPost("especialidade/{id_perito}/{id_especialidade}")]
        public IActionResult AddEspecialidade([FromRoute] int id_perito, [FromRoute] int id_especialidade)
        {
            try
            {
                peritoJuridicoService.AddEspecialidade(id_perito, id_especialidade);

                return Ok("Especialidade adicionada");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
