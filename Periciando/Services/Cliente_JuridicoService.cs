﻿using Periciando.Persistence;
using Periciando.Domain;
using System.Collections.Generic;
using System;
using System.Diagnostics;

namespace Periciando.Services
{
    public class Cliente_JuridicoService
    {
        PericiandoDAO dao = new PericiandoDAO();
        private readonly string entityName = "cliente_juridico";

        public List<Object> ReadAllClientes_Juridicos()
        {
            List<Object> clientes_juridicos = dao.SelectAll(entityName);
            List<Object> enderecos = dao.SelectAll("endereco");

            foreach (Cliente_Juridico cliente in clientes_juridicos)
            {
                foreach (Endereco endereco in enderecos)
                {
                    if (cliente.Endereco.Id == endereco.Id)
                    {
                        cliente.Endereco = endereco;
                        break;
                    }
                }
            }
            return clientes_juridicos;
        }

        public Cliente_Juridico ReadCliente_Juridico(int id)
        {
            Cliente_Juridico cliente_juridico = dao.SelectById<Cliente_Juridico>(id, entityName, new Cliente_Juridico());
            Endereco endereco = dao.SelectById<Endereco>(cliente_juridico.Endereco.Id, "endereco", new Endereco());

            endereco.Id = cliente_juridico.Endereco.Id;

            cliente_juridico.Endereco = endereco;

            cliente_juridico.Id = id;

            return cliente_juridico;
        }

        public void CreateCliente_Juridico(Cliente_Juridico cliente_juridico)
        {
            dao.InsertInheritance(cliente_juridico);
        }

        public void DeleteCliente_Juridico(int id)
        {
            dao.Delete(id, entityName);
        }

        public void UpdateSenha(int id, string senha)
        {
            dao.UpdateSenha(id, entityName, senha);
        }
    }
}
