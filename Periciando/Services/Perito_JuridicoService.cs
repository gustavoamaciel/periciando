﻿using Periciando.Persistence;
using Periciando.Domain;
using System.Collections.Generic;
using System;
using System.Diagnostics;
namespace Periciando.Services
{
    public class Perito_JuridicoService
    {
        PericiandoDAO dao = new PericiandoDAO();
        private EspecialidadeService service = new EspecialidadeService();
        private readonly string entityName = "perito_juridico";

        public List<Object> ReadAllPeritos_Juridicos()
        {
            List<Object> peritos_juridicos = dao.SelectAll(entityName);
            List<Object> enderecos = dao.SelectAll("endereco");

            foreach (Perito_Juridico perito in peritos_juridicos)
            {
                foreach (Endereco endereco in enderecos)
                {
                    if (perito.Endereco.Id == endereco.Id)
                    {
                        perito.Endereco = endereco;
                        break;
                    }
                }
            }
            return peritos_juridicos;
        }

        public Perito_Juridico ReadPerito_Juridico(int id)
        {
            Perito_Juridico perito_juridico = dao.SelectById<Perito_Juridico>(id, entityName, new Perito_Juridico());
            Endereco endereco = dao.SelectById<Endereco>(perito_juridico.Endereco.Id, "endereco", new Endereco());
            List<Object> pagamentos = Pagamento_Perito_JuridicoService.ReadPagamentoOnePerito_Juridico(dao, "pagamento_perito_juridico");
            List<Especialidade> especialidades = service.GetPeritoEspecialidades("peritos_juridicos", entityName, id);

            foreach (Pagamento_Perito_Juridico pagamento in pagamentos)
            {
                if (pagamento.Perito_Juridico.Id == id)
                {
                    pagamento.Perito_Juridico = null;
                    perito_juridico.Pagamentos.Add(pagamento);
                }
            }

            perito_juridico.Especialidades.AddRange(especialidades);

            endereco.Id = perito_juridico.Endereco.Id;

            perito_juridico.Endereco = endereco;

            perito_juridico.Id = id;

            return perito_juridico;
        }

        public void CreatePerito_Juridico(Perito_Juridico perito_juridico)
        {
            dao.InsertInheritance(perito_juridico);

        }

        public void DeletePerito_Juridico(int id)
        {
            dao.Delete(id, entityName);
        }

        public void UpdateSenha(int id, string senha)
        {
            dao.UpdateSenha(id, entityName, senha);
        }

        public void AddEspecialidade(int id_perito, int id_especialidade)
        {
            Perito_Juridico perito = new Perito_Juridico(id_perito);
            Especialidade especialidade = new Especialidade(id_especialidade);
            Perito_Juridico_Especialidade perito_especialidade = new Perito_Juridico_Especialidade(perito, especialidade);
            dao.Insert(perito_especialidade);
        }
    }

}
