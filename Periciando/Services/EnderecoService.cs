﻿using Periciando.Persistence;
using Periciando.Domain;
using System.Diagnostics;

namespace Periciando.Services
{
    public class EnderecoService
    {
        PericiandoDAO dao = new PericiandoDAO();

        public int CreateEndereco(Endereco endereco)
        {
            int newID = dao.Insert(endereco);

            Debug.WriteLine("newID" + newID);

            return newID;
        }

        public void UpdateEndereco(Endereco endereco)
        {
            dao.UpdateEndereco(endereco);
        }

        public void DeleteEndereco(int id)
        {
            dao.Delete(id, "Endereco");
        }
    }
}
