﻿using Periciando.Persistence;
using Periciando.Domain;
using System.Collections.Generic;
using System;
using System.Diagnostics;

namespace Periciando.Services
{
    public class Cliente_FisicoService
    {
        PericiandoDAO dao = new PericiandoDAO();
        private readonly string entityName = "cliente_fisico";

        public List<Object> ReadAllClientes_Fisicos()
        {
            List<Object> clientes_fisicos = dao.SelectAll(entityName);
            List<Object> enderecos = dao.SelectAll("endereco");

            foreach (Cliente_Fisico cliente in clientes_fisicos)
            {
                foreach (Endereco endereco in enderecos)
                {
                    if (cliente.Endereco.Id == endereco.Id)
                    {
                        cliente.Endereco = endereco;
                        break;
                    }
                }
            }
            return clientes_fisicos;
        }

        public Cliente_Fisico ReadCliente_Fisico(int id)
        {
            Cliente_Fisico cliente_fisico = dao.SelectById<Cliente_Fisico>(id, entityName, new Cliente_Fisico());
            Endereco endereco = dao.SelectById<Endereco>(cliente_fisico.Endereco.Id, "endereco", new Endereco());

            endereco.Id = cliente_fisico.Endereco.Id;

            cliente_fisico.Endereco = endereco;

            cliente_fisico.Id = id;

            return cliente_fisico;
        }

        public void CreateCliente_Fisico(Cliente_Fisico cliente_fisico)
        {
            dao.InsertInheritance(cliente_fisico);
        }

        public void DeleteCliente_Fisico(int id)
        {
            dao.Delete(id, entityName);
        }

        public void UpdateSenha(int id, string senha)
        {
            dao.UpdateSenha(id, entityName, senha);
        }
    }
}
