﻿using Periciando.Persistence;
using Periciando.Domain;
using System.Collections.Generic;
using System;

namespace Periciando.Services
{
    public class EspecialidadeService
    {
        PericiandoDAO dao = new PericiandoDAO();
        private readonly string entityName = "especialidade";

        public List<Object> GetPeritosByEspecialidade(int id)
        {
            List<Object> peritos = new List<Object>();
            List<Object> peritos_fisicos = dao.SelectPeritosByEspecialidade("peritos_fisicos", "perito_fisico", id);
            List<Object> peritos_juridicos = dao.SelectPeritosByEspecialidade("peritos_juridicos", "perito_juridico", id);

            peritos.AddRange(peritos_fisicos);
            peritos.AddRange(peritos_juridicos);

            return peritos;
        }

        public List<Especialidade> GetPeritoEspecialidades(string peritoType, string idType, int id)
        {
            List<Especialidade> especialidades = dao.SelectPeritoEspecialidades(peritoType, idType, id);

            return especialidades;
        }

        public void CreateEspecialidade(Especialidade especialidade)
        {
            dao.Insert(especialidade);
        }

        public void DeleteEspecialidade(int id)
        {
            dao.Delete(id, entityName);
        }
    }
}
