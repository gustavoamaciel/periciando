﻿using Periciando.Persistence;
using Periciando.Domain;
using System.Diagnostics;
using System.Collections.Generic;
using System;

namespace Periciando.Services
{
    public class Pagamento_Perito_FisicoService
    {
        PericiandoDAO dao = new PericiandoDAO();
        private Perito_FisicoService service = new Perito_FisicoService();
        private readonly string entityName = "pagamento_perito_fisico";

        public List<Object> ReadAllPagamentos_Peritos_Fisicos()
        {
            List<Object> pagamentos_peritos_fisicos = dao.SelectAll(entityName);
            List<Object> peritos_fisicos = dao.SelectAll("perito_fisico");
            List<Object> enderecos = dao.SelectAll("endereco");

            foreach (Pagamento_Perito_Fisico pagamento in pagamentos_peritos_fisicos)
            {
                foreach (Perito_Fisico perito in peritos_fisicos)
                {
                    if (pagamento.Perito_Fisico.Id == perito.Id)
                    {
                        pagamento.Perito_Fisico = perito;
                    }
                    foreach (Endereco endereco in enderecos)
                    {
                        if (perito.Endereco.Id == endereco.Id)
                        {
                            perito.Endereco = endereco;
                            break;
                        }
                    }
                }
            }
            return pagamentos_peritos_fisicos;
        }

        public static List<Object> ReadPagamentoOnePerito_Fisico(PericiandoDAO dao, string entityName)
        {
            List<Object> pagamentos_peritos_fisicos = dao.SelectAll(entityName);

            return pagamentos_peritos_fisicos;
        }

        public Pagamento_Perito_Fisico ReadPagamento_Perito_Fisico(int id)
        {
            Pagamento_Perito_Fisico pagamento_perito_fisico = dao.SelectById<Pagamento_Perito_Fisico>(id, entityName, new Pagamento_Perito_Fisico());
            Perito_Fisico perito_fisico = service.ReadPerito_Fisico(pagamento_perito_fisico.Perito_Fisico.Id);

            pagamento_perito_fisico.Perito_Fisico = perito_fisico;

            pagamento_perito_fisico.Id = id;

            return pagamento_perito_fisico;
        }

        public void CreatePagamento_Perito_Fisico(Pagamento_Perito_Fisico pagamento_perito_fisico)
        {
            dao.Insert(pagamento_perito_fisico);
        }
    }
}
