﻿using Periciando.Persistence;
using Periciando.Domain;
using System.Collections.Generic;
using System;
using System.Diagnostics;

namespace Periciando.Services
{
    public class Perito_FisicoService
    {
        PericiandoDAO dao = new PericiandoDAO();
        private EspecialidadeService service = new EspecialidadeService();
        private readonly string entityName = "perito_fisico";

        public List<Object> ReadAllPeritos_Fisicos()
        {
            List<Object> peritos_fisicos = dao.SelectAll(entityName);
            List<Object> enderecos = dao.SelectAll("endereco");

            foreach (Perito_Fisico perito in peritos_fisicos)
            {
                foreach(Endereco endereco in enderecos)
                {
                    if (perito.Endereco.Id == endereco.Id)
                    {
                        perito.Endereco = endereco;
                        break;
                    }
                }
            }
            return peritos_fisicos;
        }

        public Perito_Fisico ReadPerito_Fisico(int id)
        {
            Perito_Fisico perito_fisico = dao.SelectById<Perito_Fisico>(id, entityName, new Perito_Fisico());
            Endereco endereco = dao.SelectById<Endereco>(perito_fisico.Endereco.Id, "endereco", new Endereco());
            List<Object> pagamentos = Pagamento_Perito_FisicoService.ReadPagamentoOnePerito_Fisico(dao, "pagamento_perito_fisico");
            List<Especialidade> especialidades = service.GetPeritoEspecialidades("peritos_fisicos", entityName, id);

            foreach (Pagamento_Perito_Fisico pagamento in pagamentos)
            {
                if(pagamento.Perito_Fisico.Id == id)
                {
                    pagamento.Perito_Fisico = null;
                    perito_fisico.Pagamentos.Add(pagamento);
                }
            }

            perito_fisico.Especialidades.AddRange(especialidades);

            endereco.Id = perito_fisico.Endereco.Id;

            perito_fisico.Endereco = endereco;

            perito_fisico.Id = id;

            return perito_fisico;
        }

        public void CreatePerito_Fisico(Perito_Fisico perito_fisico)
        {
            dao.InsertInheritance(perito_fisico);

        }

        public void DeletePerito_Fisico(int id)
        {
            dao.Delete(id, entityName);
        }

        public void UpdateSenha(int id, string senha)
        {
            dao.UpdateSenha(id, entityName, senha);
        }

        public void AddEspecialidade(int id_perito, int id_especialidade)
        {
            Perito_Fisico perito = new Perito_Fisico(id_perito);
            Especialidade especialidade = new Especialidade(id_especialidade);
            Perito_Fisico_Especialidade perito_especialidade = new Perito_Fisico_Especialidade(perito, especialidade);
            dao.Insert(perito_especialidade);
        }
    }
}
