﻿using Periciando.Persistence;
using Periciando.Domain;
using System.Diagnostics;
using System;
using System.Collections.Generic;


namespace Periciando.Services
{
    public class Pagamento_Perito_JuridicoService
    {
        PericiandoDAO dao = new PericiandoDAO();
        private Perito_JuridicoService service = new Perito_JuridicoService();
        private readonly string entityName = "pagamento_perito_juridico";

        public List<Object> ReadAllPagamentos_Peritos_Juridicos()
        {
            List<Object> pagamentos_peritos_juridicos = dao.SelectAll(entityName);
            List<Object> peritos_juridicos = dao.SelectAll("perito_juridico");
            List<Object> enderecos = dao.SelectAll("endereco");

            foreach (Pagamento_Perito_Juridico pagamento in pagamentos_peritos_juridicos)
            {
                foreach (Perito_Juridico perito in peritos_juridicos)
                {
                    if (pagamento.Perito_Juridico.Id == perito.Id)
                    {
                        pagamento.Perito_Juridico = perito;
                    }
                    foreach (Endereco endereco in enderecos)
                    {
                        if (perito.Endereco.Id == endereco.Id)
                        {
                            perito.Endereco = endereco;
                            break;
                        }
                    }
                }
            }
            return pagamentos_peritos_juridicos;
        }

        public static List<Object> ReadPagamentoOnePerito_Juridico(PericiandoDAO dao, string entityName)
        {
            List<Object> pagamentos_peritos_juridicos = dao.SelectAll(entityName);

            return pagamentos_peritos_juridicos;
        }

        public Pagamento_Perito_Juridico ReadPagamento_Perito_Juridico(int id)
        {
            Pagamento_Perito_Juridico pagamento_perito_juridico = dao.SelectById<Pagamento_Perito_Juridico>(id, entityName, new Pagamento_Perito_Juridico());
            Perito_Juridico perito_juridico = service.ReadPerito_Juridico(pagamento_perito_juridico.Perito_Juridico.Id);

            pagamento_perito_juridico.Perito_Juridico = perito_juridico;

            pagamento_perito_juridico.Id = id;

            return pagamento_perito_juridico;
        }

        public void CreatePagamento_Perito_Juridico(Pagamento_Perito_Juridico pagamento_perito_juridico)
        {
            dao.Insert(pagamento_perito_juridico);
        }
    }
}
