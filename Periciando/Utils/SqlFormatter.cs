﻿using System.Reflection;
using System;
using System.Text;
using Periciando.Domain;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Diagnostics;

namespace Periciando.Utils
{
    public class SqlFormatter
    {
        // Get Class attributes names in string format i.e (id, nome, email)
        private static StringBuilder GetClassAttributesNames(PropertyInfo[] properties)
        {
            StringBuilder fields = new StringBuilder();
            foreach (PropertyInfo property in properties)
            {

                if (property.Name == "Id" || property.ToString().Contains("List"))
                {
                    continue;
                }

                if (property.ToString().Contains("Periciando"))
                {
                    fields.Append($"id_{property.Name.ToLower()}, ");
                }
                else
                {
                    fields.Append($"{property.Name.ToLower()}, ");
                }

            }
            return fields;
        }

        private static string GenerateEntityName(string entityName)
        {
            int fixDoubleUnderscore = 0;
            StringBuilder formatedEntityName = new StringBuilder();
            if (entityName.Contains('_'))
            {
                for (int i = 0; i < entityName.Length; i++)
                {
                    formatedEntityName.Append(entityName[i]);
                    if (entityName[i] == '_')
                    {
                        formatedEntityName.Insert(i + fixDoubleUnderscore, 's');
                        fixDoubleUnderscore++;
                    }
                }
            }
            else
            {
                formatedEntityName.Append(entityName);
            }

            return formatedEntityName.ToString().ToLower();
        }

        private static StringBuilder GenerateParamValues(PropertyInfo[] properties)
        {
            StringBuilder fields = new StringBuilder();
            foreach (PropertyInfo property in properties)
            {
                if (property.Name == "Id" || property.ToString().Contains("List"))
                {
                    continue;
                }
                if (property.ToString().Contains("Periciando"))
                {
                    fields.Append($"@Id_{property.Name},");
                }
                else
                {
                    fields.Append($"@{property.Name},");
                }

            }
            return fields;
        }

        public static List<MySqlParameter> GenerateMySqlParams(Object obj)
        {
            Type type = obj.GetType();
            PropertyInfo[] properties = type.GetProperties();
            FieldInfo[] fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            List<MySqlParameter> mySqlParameters = new List<MySqlParameter>();

            foreach (PropertyInfo property in properties)
            {
                foreach (FieldInfo field in fields)
                {
                    if (property.Name == "Id" || property.ToString().Contains("List"))
                    {
                        continue;
                    }

                    if (field.ToString().Contains(property.Name))
                    {
                        mySqlParameters.Add(new MySqlParameter(property.Name, field.GetValue(obj)));
                        break;
                    }
                }

                if (property.ToString().Contains("Periciando"))
                {
                    Type typePericiando = property.GetValue(obj).GetType();
                    FieldInfo[] typeFields = typePericiando.BaseType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
                    FieldInfo[] fieldsPericiando = typePericiando.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
                    
                    foreach (FieldInfo field in fieldsPericiando)
                    {
                        if (field.ToString().Contains("Int") && field.ToString().Contains("Id"))
                        {
                            mySqlParameters.Add(new MySqlParameter($"Id_{property.Name}", field.GetValue(property.GetValue(obj))));
                            break;
                        }
                    }

                    foreach (FieldInfo nestedFieldType in typeFields)
                    {
                        if (nestedFieldType.ToString().Contains("Int") && nestedFieldType.ToString().Contains("Id"))
                        {
                            mySqlParameters.Add(new MySqlParameter($"Id_{property.Name}", nestedFieldType.GetValue(property.GetValue(obj))));
                            break;
                        }
                    }

                }


            }
            return mySqlParameters;
        }

        // Get inheritance object params to pass to MySql driver
        public static List<MySqlParameter> GenerateMySqlParamsInheritance(Object obj)
        {
            Type type = obj.GetType();
            PropertyInfo[] properties = type.GetProperties();
            FieldInfo[] fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo[] inheritanceFields = type.BaseType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            List<MySqlParameter> mySqlParameters = new List<MySqlParameter>();

            foreach (PropertyInfo property in properties)
            {
                foreach (FieldInfo field in fields)
                {
                    if (property.Name == "Id" || property.ToString().Contains("List"))
                    {
                        continue;
                    }

                    if (field.ToString().Contains(property.Name))
                    {
                        mySqlParameters.Add(new MySqlParameter(property.Name, field.GetValue(obj)));
                        break;
                    }
                }

                foreach (FieldInfo inheritanceField in inheritanceFields)
                {
                    if (property.Name == "Id" || property.ToString().Contains("List"))
                    {
                        continue;
                    }

                    if (inheritanceField.ToString().Contains(property.Name))
                    {
                        if (property.ToString().Contains("Periciando"))
                        {
                            Type nestedType = inheritanceField.GetValue(obj).GetType();
                            FieldInfo[] nestedFieldTypes = nestedType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);

                            foreach (FieldInfo nestedFieldType in nestedFieldTypes)
                            {
                                if (nestedFieldType.ToString().Contains("Int") && nestedFieldType.ToString().Contains("Id"))
                                {
                                    mySqlParameters.Add(new MySqlParameter($"Id_{property.Name}", nestedFieldType.GetValue(inheritanceField.GetValue(obj))));
                                    break;
                                }
                            }
                            continue;
                        }
                        else
                        {
                            mySqlParameters.Add(new MySqlParameter(property.Name, inheritanceField.GetValue(obj)));
                        }
                    }
                }
            }
            return mySqlParameters;
        }

        public static string GenerateInsertString(Object obj)
        {
            Type type = obj.GetType();
            PropertyInfo[] properties = type.GetProperties();

            string entityName = GenerateEntityName(type.Name.ToString());

            StringBuilder campos = SqlFormatter.GetClassAttributesNames(properties);
            StringBuilder valores = SqlFormatter.GenerateParamValues(properties);

            // Remove last comma and space from field names
            campos.Remove(campos.ToString().Length - 2, 2).ToString();
            valores.Remove(valores.ToString().Length - 1, 1).ToString();

            return $"INSERT INTO {entityName}s ({campos}) VALUES ({valores})";
        }

        public static string GenerateDeleteString(string entityName)
        {
            string formattedEntityName = GenerateEntityName(entityName);
            string query = $"DELETE FROM {formattedEntityName}s WHERE id_{entityName} = @Id;";

            return query;
        }

        public static string GenerateUpdateSenhaString(string entityName)
        {
            string formattedEntityName = GenerateEntityName(entityName);
            string query = $"UPDATE {formattedEntityName}s SET senha = @Senha WHERE id_{entityName} = @Id;";

            return query;
        }

        public static string GenerateUpdateEnderecoString()
        {
            string query = $"UPDATE enderecos SET cep = @Cep, rua = @Rua, bairro = @Bairro, cidade = @Cidade, pais = @Pais WHERE id_endereco = @Id;";

            return query;
        }

        public static string GenerateSelectByIdString(string entityName)
        {
            string formattedEntityName = GenerateEntityName(entityName);

            string query = $"SELECT * FROM {formattedEntityName}s WHERE id_{entityName} = @Id";

            return query;
        }

        public static string GenerateSelectAllString(string entityName)
        {
            string formattedEntityName = GenerateEntityName(entityName);

            string query = $"SELECT * FROM {formattedEntityName}s;";

            return query;
        }

        public static string GenerateSelectEspecialidadesString(string peritoType, string idType)
        {
            string query = $"SELECT especialidades.id_especialidade,nome FROM especialidades INNER JOIN {peritoType}_especialidades ON {peritoType}_especialidades.id_{idType} = @Id WHERE especialidades.id_especialidade IN ({peritoType}_especialidades.id_especialidade) GROUP BY especialidades.nome;";

            return query;
        }

        public static string GenerateSelectPeritosByEspecialidadeString(string peritoType, string idType)
        {
            string query = $"SELECT {peritoType}.id_{idType}, {peritoType}.email, {peritoType}.telefone, {peritoType}.nome FROM {peritoType} INNER JOIN {peritoType}_especialidades ON {peritoType}_especialidades.id_especialidade = @Id WHERE {peritoType}.id_{idType} IN ({peritoType}_especialidades.id_{idType}) GROUP BY {peritoType}.nome;";

            return query;
        }
    }
}
